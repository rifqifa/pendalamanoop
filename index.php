<?php
require_once "Class/Elang.php";
require_once "Class/Harimau.php";

Echo "<h3> Battle </h3>";
Echo "<h4> Player 1 </h4>";
$elang = New Elang(1);
Echo $elang->getInfoHewan();
Echo $elang->atraksi();

Echo "<h4> Player 2 </h4>";
$harimau = New Harimau(1);
Echo $harimau->getInfoHewan();
Echo $harimau->atraksi();

Echo "<h4> Fight! </h4>";
Echo $harimau->serang($elang);
Echo $elang->diserang($harimau);
Echo $elang->serang($harimau);
Echo $harimau->diserang($elang);

?>