<?php

    require_once "Traits/Hewan.php";
    require_once "Traits/Fight.php";

    class Harimau {
        use Fight, Hewan;
        public $jenis_hewan="Harimau";
        public function __construct ($nama) {
            $this->nama = "Harimau " . $nama;
        }

        public function getInfoHewan() {
            $this->keahlian = "lari cepat";
            $this->jumlah_kaki = 4;
            $this->attackPower = 7;
            $this->defencePower = 8;
            echo "nama = " . $this->nama . ", jenis hewan = " . $this->jenis_hewan . ", keahlian = " . $this->keahlian . ", jumlah kaki = " . $this->jumlah_kaki . ", attack power = " . $this->attackPower . ", defence power = " . $this->defencePower . "<br>" ;
            
        }

        
    }

