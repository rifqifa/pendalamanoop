<?php

    require_once "Traits/Hewan.php";
    require_once "Traits/Fight.php";

    class Elang {
        use Fight, Hewan;
        public $jenis_hewan = "Elang";
        public function __construct ($nama) {
            $this->nama = "elang " . $nama;
        }

        public function getInfoHewan() {
            $this->keahlian = "terbang tinggi";
            $this->jumlah_kaki = 2;
            $this->attackPower = 10;
            $this->defencePower = 5;
            echo "nama = " . $this->nama . ", jenis hewan = " . $this->jenis_hewan . ", keahlian = " . $this->keahlian . ", jumlah kaki = " . $this->jumlah_kaki . ", attack power = " . $this->attackPower . ", defence power = " . $this->defencePower . "<br>" ;
            
        }

    }


?>